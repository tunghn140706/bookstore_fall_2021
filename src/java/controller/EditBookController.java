/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.bookDAO.BookDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.book.Book;

/**
 *
 * @author Nam Ngoc
 */
@WebServlet(name = "EditBookController", urlPatterns = {"/editBook"})
public class EditBookController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
           int id = Integer.parseInt(request.getParameter("id"));
            BookDAOImpl bookDAO = new BookDAOImpl();
            Book b = bookDAO.getBookById(id);
            request.setAttribute("product", b);
            request.getRequestDispatcher("editProductInfo.jsp").forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(EditBookController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EditBookController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
            int id =Integer.parseInt(request.getParameter("id"));
            String title = request.getParameter("title");
            String description = request.getParameter("description");
            String content = request.getParameter("content");
            String author = request.getParameter("author");
            Book book = new Book(id,title, description, content, author);
            
            BookDAOImpl bookDAO = new BookDAOImpl();
            bookDAO.updateBook(book);
            response.sendRedirect("alter");
        } catch (SQLException ex) {
            Logger.getLogger(AddBookController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AddBookController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
