/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.order;

import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class Cart {
    private int id;
    private int customerId;
    private HashMap<Integer, Integer> itemBook;

    public Cart() {
    }

    public Cart(int customerId, int id, HashMap<Integer, Integer> itemBook) {
        this.id = id;
        this.customerId = customerId;
        this.itemBook = itemBook;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HashMap<Integer, Integer> getItemBook() {
        return itemBook;
    }

    public void setItemBook(HashMap<Integer, Integer> itemBook) {
        this.itemBook = itemBook;
    }
    
    
}
