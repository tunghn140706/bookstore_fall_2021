/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.order;

import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class Order {
    private int id;
    private int customerId;
    private Payment payment;
    private Shipment shipment;
    private HashMap<Integer, Integer> itemBook;

    public Order() {
    }

    public Order(Payment payment, Shipment shipment, int customerId, int id, HashMap<Integer, Integer> itemBook) {
        this.id = id;
        this.shipment = shipment;
        this.payment = payment;
        this.customerId = customerId;
        this.itemBook = itemBook;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }
    
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HashMap<Integer, Integer> getItemBook() {
        return itemBook;
    }

    public void setItemBook(HashMap<Integer, Integer> itemBook) {
        this.itemBook = itemBook;
    }
}
