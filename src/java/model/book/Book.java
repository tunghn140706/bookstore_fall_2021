/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.book;

/**
 *
 * @author Admin
 */
public class Book {
    private int id;
    private String title;
    private String description;
    private String content;
    private String author;
    
    public Book() {
    }
    
    public Book(String title, String description, String content, String author) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.content = content;
        this.author = author;
    }

    public Book(int id, String title, String description, String content, String author) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.content = content;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
