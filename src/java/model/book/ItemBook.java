/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.book;

/**
 *
 * @author Admin
 */
public class ItemBook {
    private int itemBookId;
    private int bookId;
    private int quantity;

    public ItemBook() {
    }

    public ItemBook(int bookId, int quantity) {
        this.itemBookId = itemBookId;
        this.bookId = bookId;
        this.quantity = quantity;
    }

    public ItemBook(int itemBookId, int bookId, int quantity) {
        this.itemBookId = itemBookId;
        this.bookId = bookId;
        this.quantity = quantity;
    }

    public int getItemBookId() {
        return itemBookId;
    }

    public void setItemBookId(int itemBookId) {
        this.itemBookId = itemBookId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

  
    
    
}
