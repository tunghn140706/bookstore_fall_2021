/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.orderDAO;

import context.DBContext;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.order.Order;
import model.order.Payment;
import model.order.Shipment;

/**
 *
 * @author cuongnm
 */
public class OrderDAOImp implements OrderDAO {

    @Override
    public List<Order> getAllOrder() throws Exception {
        List<Order> list = new ArrayList<>();
        DBContext db = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PaymentDAO paymentDAO = null;
        ShipmentDAO shipmentDAO = null;
        ItemBookDAO itemBookDAO = null;
        String sql = "SELECT * FROM [Order]";
        try {
            paymentDAO = new PaymentDAOImp();
            shipmentDAO = new ShipmentDAOImp();
            itemBookDAO = new ItemBookDAOImp();
            db = new DBContext();
            conn = db.getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt(1);
                int customerId = rs.getInt(3);
                Payment payment = paymentDAO.getPaymentByOrder(id);
                Shipment shipment = shipmentDAO.getShipmentByOrder(id);
                HashMap<Integer, Integer> itemBook = itemBookDAO.getItemBookByOrder(id);
                list.add(new Order(payment,
                        shipment,
                        customerId,
                        id,
                        itemBook));
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            db.closeConnection(conn, ps, rs);
        }
    }

    @Override
    public Order getOrderById(int orderId) throws Exception {
        DBContext db = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PaymentDAO paymentDAO = null;
        ShipmentDAO shipmentDAO = null;
        ItemBookDAO itemBookDAO = null;
        String sql = "SELECT * FROM [Order] where id = ?";
        try {
            paymentDAO = new PaymentDAOImp();
            shipmentDAO = new ShipmentDAOImp();
            itemBookDAO = new ItemBookDAOImp();
            db = new DBContext();
            conn = db.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, orderId);
            rs = ps.executeQuery();

            if (rs.next()) {
                int id = rs.getInt(1);
                int customerId = rs.getInt(3);
                Payment payment = paymentDAO.getPaymentByOrder(id);
                Shipment shipment = shipmentDAO.getShipmentByOrder(id);
                HashMap<Integer, Integer> itemBook = itemBookDAO.getItemBookByOrder(id);
                return new Order(payment,
                        shipment,
                        customerId,
                        id,
                        itemBook);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            db.closeConnection(conn, ps, rs);
        }
    }

    @Override
    public void addOrder(Order order) throws Exception {
        DBContext db = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PaymentDAO paymentDAO = null;
        ShipmentDAO shipmentDAO = null;
        ItemBookDAO itemBookDAO = null;
        String sql = "INSERT INTO [Order] (Shipment_id, Customer_id) VALUES (?, ?, ?)";
        try {
            paymentDAO = new PaymentDAOImp();
            shipmentDAO = new ShipmentDAOImp();
            itemBookDAO = new ItemBookDAOImp();
            db = new DBContext();
            conn = db.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, order.getShipment().getId());
            ps.setInt(1, order.getCustomerId());

            int result = ps.executeUpdate();

            return;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            db.closeConnection(conn, ps, rs);
        }
    }

    @Override
    public void editOrder(Order order) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteOrder(int orderId) throws Exception {
        DBContext db = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PaymentDAO paymentDAO = null;
        ShipmentDAO shipmentDAO = null;
        ItemBookDAO itemBookDAO = null;
        String sql = "DELETE FROM [Order] where id = ?";
        try {
            paymentDAO = new PaymentDAOImp();
            shipmentDAO = new ShipmentDAOImp();
            itemBookDAO = new ItemBookDAOImp();
            db = new DBContext();
            conn = db.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, orderId);
            int result = ps.executeUpdate();

            return;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            db.closeConnection(conn, ps, rs);
        }
    }

}
