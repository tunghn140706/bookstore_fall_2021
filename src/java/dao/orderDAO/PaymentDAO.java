/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.orderDAO;

import model.order.Payment;

/**
 *
 * @author cuongnm
 */
public interface PaymentDAO {
    
    Payment getPaymentByOrder(int orderId) throws Exception;
}
