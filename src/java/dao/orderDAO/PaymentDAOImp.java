/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.orderDAO;

import context.DBContext;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.order.Payment;
import model.order.PaymentMethod;

/**
 *
 * @author cuongnm
 */
public class PaymentDAOImp implements PaymentDAO{

    @Override
    public Payment getPaymentByOrder(int orderId) throws Exception {
        DBContext db = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM dbo.Payment \n" +
            "WHERE OrderOrder_id = ?";
        try {
            db = new DBContext();
            conn = db.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, orderId);
            rs = ps.executeQuery();

            while (rs.next()) {
                return new Payment(rs.getInt(1), 
                        PaymentMethod.COD,
                        rs.getDate(5));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            db.closeConnection(conn, ps, rs);
        }
        return null;
    }
    
}
