/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.orderDAO;

import java.util.HashMap;

/**
 *
 * @author cuongnm
 */
public interface ItemBookDAO {
    
    HashMap<Integer, Integer> getItemBookByOrder(int orderID) throws Exception;
    
}
