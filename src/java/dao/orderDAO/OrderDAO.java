/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.orderDAO;

import java.util.List;
import model.order.Order;

/**
 *
 * @author cuongnm
 */
public interface OrderDAO {
    
    List<Order> getAllOrder() throws Exception;
    Order getOrderById(int id) throws Exception;
    void addOrder(Order order) throws Exception;
    void editOrder(Order order) throws Exception;
    void deleteOrder(int id) throws Exception;
    
}
