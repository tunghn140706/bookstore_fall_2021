/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.itemBookDAO;

import java.sql.SQLException;
import java.util.List;
import model.book.ItemBook;

/**
 *
 * @author Nam Ngoc
 */
public interface ItemBookDAO {
    List<ItemBook> findAllItemBook() throws SQLException, ClassNotFoundException;

    ItemBook getItemBookById(int itemBookId) throws SQLException, ClassNotFoundException;

    boolean insertItemBook(ItemBook itemBook) throws SQLException, ClassNotFoundException;

    boolean deleteItemBook(int itemBookId) throws SQLException, ClassNotFoundException;

    boolean updateItemBook(ItemBook itemBook) throws SQLException, ClassNotFoundException;
}
