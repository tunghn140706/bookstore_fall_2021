/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.itemBookDAO;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.book.ItemBook;

/**
 *
 * @author Nam Ngoc
 */
public class ItemBookDAOImpl implements ItemBookDAO {

    @Override
    public List<ItemBook> findAllItemBook() throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBContext.getConnection();

            preparedStatement = connection.prepareStatement("SELECT * FROM dbo.ItemBook");
            ResultSet resultSet = preparedStatement.executeQuery();
            ItemBook itemBook = null;
            List<ItemBook> itemBooks = new ArrayList<>();
            while (resultSet.next()) {
                itemBook = new ItemBook(resultSet.getInt(1), resultSet.getInt(2),resultSet.getInt(3));
                itemBooks.add(itemBook);
            }
            return itemBooks;
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public ItemBook getItemBookById(int itemBookId) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBContext.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM dbo.ItemBook WHERE Id = ?");
            preparedStatement.setInt(1, itemBookId);
            ResultSet resultSet = preparedStatement.executeQuery();
            ItemBook itemBook = null;
            while (resultSet.next()) {
                itemBook = new ItemBook(resultSet.getInt(1), resultSet.getInt(2));
            }
            return itemBook;
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public boolean insertItemBook(ItemBook itemBook) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBContext.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO [dbo].[ItemBook]([BookId],[Quantity]) VALUES (?,?)");
            preparedStatement.setInt(1, itemBook.getBookId());
            preparedStatement.setInt(2, itemBook.getQuantity());
            return preparedStatement.executeUpdate() > 0;
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public boolean deleteItemBook(int itemBookId) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBContext.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM dbo.ItemBook WHERE Id = ?");
            preparedStatement.setInt(1, itemBookId);
            return preparedStatement.executeUpdate() > 0;
        } catch (Exception exception) {
            exception.printStackTrace();
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public boolean updateItemBook(ItemBook itemBook) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBContext.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE [dbo].[ItemBook] SET [BookId] = ?,[Quantity] = ? WHERE Id = ?");
            preparedStatement.setInt(1, itemBook.getBookId());
            preparedStatement.setInt(2, itemBook.getQuantity());
            preparedStatement.setInt(3, itemBook.getItemBookId());
            return preparedStatement.executeUpdate() > 0;
        } catch (Exception exception) {
            exception.printStackTrace();
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        
        ItemBookDAOImpl dao = new ItemBookDAOImpl();
        List<ItemBook> findAllItemBook = dao.findAllItemBook();
        System.out.println(findAllItemBook);
        System.out.println(dao.getItemBookById(1));
//        dao.insertItemBook(new ItemBook(1,60));
//        dao.updateItemBook(new ItemBook(8,1,50));
        dao.deleteItemBook(8);

    }
}
