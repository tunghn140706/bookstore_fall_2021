/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.acccountDAO;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.account.Account;
import model.account.Role;

/**
 *
 * @author PC
 */
public class AccountDAOImpl implements AccountDAO{

    @Override
    public Account getAccountByUsername(String Username) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBContext.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM dbo.Account WHERE Username = ?");
            preparedStatement.setString(1, Username);
            ResultSet resultSet = preparedStatement.executeQuery();
            Account acc = null;
            while (resultSet.next()) {
                Role role = Role.CUSTOMER;
                if(resultSet.getInt(5) == 1){
                    role = Role.ADMIN;
                } 
                acc = new Account(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3),
                        resultSet.getString(4), role);
            }
            return acc;
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }
    
}
