/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.acccountDAO;

import java.sql.SQLException;
import model.account.Account;

/**
 *
 * @author PC
 */
public interface AccountDAO {
    
    Account getAccountByUsername(String Username) throws SQLException, ClassNotFoundException;
    
}
