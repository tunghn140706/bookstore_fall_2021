/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.bookDAO;

import context.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.book.Book;

/**
 *
 * @author Nam Ngoc
 */
public class BookDAOImpl implements BookDAO {

    @Override
    public List<Book> findAllBook() throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBContext.getConnection();

            preparedStatement = connection.prepareStatement("SELECT * FROM dbo.Book");
            ResultSet resultSet = preparedStatement.executeQuery();
            Book book = null;
            List<Book> books = new ArrayList<>();
            while (resultSet.next()) {
                book = new Book(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
                        resultSet.getString(4), resultSet.getString(5));
                books.add(book);
            }
            return books;
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public Book getBookById(int bookId) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBContext.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM dbo.Book WHERE Id = ?");
            preparedStatement.setInt(1, bookId);
            ResultSet resultSet = preparedStatement.executeQuery();
            Book book = null;
            while (resultSet.next()) {
                book = new Book(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
                        resultSet.getString(4), resultSet.getString(5));
            }
            return book;
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public boolean insertBook(Book book) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBContext.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO [dbo].[Book]([Title],[Description],[Content],[Author]) VALUES(?,?,?,?)");
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getDescription());
            preparedStatement.setString(3, book.getContent());
            preparedStatement.setString(4, book.getAuthor());
            return preparedStatement.executeUpdate() > 0;
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public boolean deleteBook(int bookId) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBContext.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM dbo.Book WHERE Id = ?");
            preparedStatement.setInt(1, bookId);
            return preparedStatement.executeUpdate() > 0;
        } catch (Exception exception) {
            exception.printStackTrace();
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public boolean updateBook(Book book) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBContext.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE [dbo].[Book] SET [Title] = ?,[Description] = ?,[Content] = ?,[Author] = ? WHERE Id = ?");
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getDescription());
            preparedStatement.setString(3, book.getContent());
            preparedStatement.setString(4, book.getAuthor());
            preparedStatement.setInt(5, book.getId());
            return preparedStatement.executeUpdate() > 0;
        } catch (Exception exception) {
            exception.printStackTrace();
            throw exception;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        BookDAOImpl dao = new BookDAOImpl();
        List<Book> findAllBook = dao.findAllBook();
        System.out.println(findAllBook);
        System.out.println(dao.getBookById(1));
//        dao.insertBook(new Book("Title6", "Description6", "Content6", "Author6"));
//        dao.updateBook(new Book(6, "Book6", "Description7", "Content8", "Author9"));
//        dao.deleteBook(6);
    }
}
