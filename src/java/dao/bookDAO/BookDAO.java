/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.bookDAO;

import java.sql.SQLException;
import java.util.List;
import model.book.Book;

/**
 *
 * @author Nam Ngoc
 */
public interface BookDAO {

    List<Book> findAllBook() throws SQLException, ClassNotFoundException;

    Book getBookById(int bookId) throws SQLException, ClassNotFoundException;

    boolean insertBook(Book book) throws SQLException, ClassNotFoundException;

    boolean deleteBook(int bookId) throws SQLException, ClassNotFoundException;

    boolean updateBook(Book book) throws SQLException, ClassNotFoundException;
}
