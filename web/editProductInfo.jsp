<%-- 
    Document   : editProductInfoPage
    Created on : Nov 18, 2020, 10:06:20 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <div class="navbar">
                <div class="logo">
                    <img src="images/logo.png" width="125px">
                </div>
                <nav>
                    <ul id="MenuItems"> 
                        <li><a href="Home">Home</a></li>
                        <li><a href="allProduct">Product</a></li>
                        <li><a href="">About</a></li>
                        <li><a href="">Contact</a></li>
                        <li><a href="logout">Log Out</a></li>
                    </ul>
                </nav>
                <a href="CartServlet"><img src="images/cart.png" width="30px" height="30px"></a>
                <img src="images/menu.png" class="menu-icon" onclick="menutoggle()">

            </div>
        </div>
        <div class="add-page my-5 text-center" style="background-color: #ff523b; color: #fff;">
            <h1><strong>Update Product</strong></h1>
        </div>
        <form action="editBook" method="post">
            <table class="table table-borderless col-sm-12 mx-2 ">
                <input type="hidden" id="idCustomer" name="id" value="${product.id}">
                <tr>
                    <td>
                        <h4 class="text-left"><strong>TITLE</strong></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="title" placeholder="Enter Title" value="${product.title}" style ="width: 100%">
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="text-left"><strong>DESCRIPTION</strong></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="description" placeholder="Enter Description" value="${product.description}" style ="width: 100%">
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="text-left"><strong>CONTENT</strong></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="content" placeholder="Enter Content" value="${product.content}" style ="width: 100%">
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="text-left"><strong>Author</strong></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="author" placeholder="Enter Author" value="${product.author}" style ="width: 100%">
                    </td>
                </tr>
            </table>
            <div class="submit-btn mx-2 my-4" style="text-align: center; align-items: center">
                <input type="submit" name="btnUpdate" value="Update" class="btn my-0 col-2">
            </div>
        </form>

        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-col-1">
                        <h3>Download Our App</h3>
                        <p>Download App For Android and IOS Mobile Phone.</p>
                        <div class="app-logo">
                            <img src="images/play-store.png">
                            <img src="images/app-store.png">
                        </div>
                    </div>
                    <div class="footer-col-2">
                        <img src="images/logo-white.png" >
                        <p>Our Purpose is Subtainably Make The Pleasure and Benefits Of Sports Accessible to the Many.</p>
                    </div>
                    <div class="footer-col-3">
                        <h3>Useful Links</h3>
                        <ul>
                            <li>Coupons</li>
                            <li>Blog Post</li>
                            <li>Return Policy</li>
                            <li>Join Affiliate</li>
                        </ul>
                    </div>
                    <div class="footer-col-4">
                        <h3>Follow Us</h3>
                        <ul>
                            <li>Facebook</li>
                            <li>Twitter</li>
                            <li>Instagram</li>
                            <li>Youtube</li>
                        </ul>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <script>
            var MenuItems = document.getElementById("MenuItems");
            MenuItems.style.maxHeight = "0px";
            function menutoggle() {
                if (MenuItems.style.maxHeight == "0px") {
                    MenuItems.style.maxHeight = "200px";
                } else {
                    MenuItems.style.maxHeight = "0px";
                }
            }
        </script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    </body>
</html>
