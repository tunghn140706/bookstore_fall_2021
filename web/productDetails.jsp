<%-- 
    Document   : productDetails
    Created on : Nov 4, 2020, 8:27:45 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset = "UTF-8">
        <meta name="viewport" content= "width = device-width, initial-scale=1.0">
        <title>All products - Tung Store</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>

        <div class="container">
            <div class="navbar">
                <div class="logo">
                    <img src="images/logo.png" width="125px">
                </div>
                <nav>
                    <ul id="MenuItems"> 
                        <li><a href="Home">Home</a></li>
                        <li><a href="allProduct">Product</a></li>
                        <li><a href="">About</a></li>
                        <li><a href="">Contact</a></li>
                        <li><a href="logout">Log Out</a></li>
                    </ul>
                </nav>
                <a href="CartServlet"><img src="images/cart.png" width="30px" height="30px"></a>
                <img src="images/menu.png" class="menu-icon" onclick="menutoggle()">

            </div>
        </div>


        <!------ Single product details--------->
        <div class="small-container1 single-product">
            <div class="row">
                <div class="col-2">
                    <img src="${requestScope.product.img}" width="95%" id="productImg">
                </div>
                <div class ="col-2">
                    <form action="CartServlet" method="post">
                        <p>Home / ${requestScope.product.productType}</p>
                        <h1>${requestScope.product.name}</h1>
                        <h4>$${requestScope.product.price}</h4>
                        <select>
                            <option>Select Size</option>
                            <option>XS</option>
                            <option>S</option>
                            <option>M</option>
                            <option>L</option>
                            <option>XL</option>
                        </select>
                        <br>
                        <input type="number" name="quantity" value = "1" class="inputBox">
                        <input type="hidden" name="ProductId" class="inputBox" value = ${requestScope.product.id}>
                        <br>
<!--                        <a href="CartServlet?ProductId=${requestScope.product.id}" class="btn">Add to Cart</a>-->
                        <input type="submit" name="Submit" value="Add to Cart" class ="btn">
                        <h3>Product Details <i class="fa fa-indent"></i></h3>
                        <p>${requestScope.product.description}</p>
                    </form>
                </div>
            </div>
        </div>

        <!-- -----title------ -->
    <divs class ="small-container1">
        <div class="row row-2">
            <h2>Related Products</h2>
            <a href="allProduct">View More</a>
        </div>
    </div>



    <!-- --------Suggested products----------
    -->	<div class="small-container1">

        <div class="row ">

            <div class="col-4">
                <img src="images/product-12.jpg">
                <h4>Red Printed T-Shirt</h4>
                <div class="Rating">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star-o" aria-hidden="true"></i>
                </div>
                <p>$50.00</p>
            </div>
            <div class="col-4">
                <img src="images/product-9.jpg">
                <h4>Red Printed T-Shirt</h4>
                <div class="Rating">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star-o" aria-hidden="true"></i>
                </div>
                <p>$50.00</p>
            </div>
            <div class="col-4">
                <img src="images/product-10.jpg">
                <h4>Red Printed T-Shirt</h4>
                <div class="Rating">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                </div>
                <p>$50.00</p>
            </div>
            <div class="col-4">
                <img src="images/product-11.jpg">
                <h4>Red Printed T-Shirt</h4>
                <div class="Rating">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star-o" aria-hidden="true"></i>
                </div>
                <p>$50.00</p>
            </div>

        </div>



        <!-- ----------footer----------- -->
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-col-1">
                        <h3>Download Our App</h3>
                        <p>Download App For Android and IOS Mobile Phone.</p>
                        <div class="app-logo">
                            <img src="images/play-store.png">
                            <img src="images/app-store.png">
                        </div>
                    </div>
                    <div class="footer-col-2">
                        <img src="images/logo-white.png" >
                        <p>Our Purpose is Subtainably Make The Pleasure and Benefits Of Sports Accessible to the Many.</p>
                    </div>
                    <div class="footer-col-3">
                        <h3>Useful Links</h3>
                        <ul>
                            <li>Coupons</li>
                            <li>Blog Post</li>
                            <li>Return Policy</li>
                            <li>Join Affiliate</li>
                        </ul>
                    </div>
                    <div class="footer-col-4">
                        <h3>Follow Us</h3>
                        <ul>
                            <li>Facebook</li>
                            <li>Twitter</li>
                            <li>Instagram</li>
                            <li>Youtube</li>
                        </ul>
                    </div>
                </div>
                <hr>
                <p class="copyRight">Copyright 2020- Easy Tutorials</p>
            </div>
        </div>


        <!-- ---------- js for toggle menu----------- -->
        <script>
            var MenuItems = document.getElementById("MenuItems");
            MenuItems.style.maxHeight = "0px";
            function menutoggle() {
                if (MenuItems.style.maxHeight == "0px") {
                    MenuItems.style.maxHeight = "200px";
                } else {
                    MenuItems.style.maxHeight = "0px";
                }
            }
        </script>


        <!-- ---------JS for product gallery----------->
        <script>
            var productImg = document.getElementById("productImg");
            var smallImg = document.getElementsByClassName("small-img");
            smallImg[0].onclick = function () {
                productImg.src = smallImg[0].src;
            }
            smallImg[1].onclick = function () {
                productImg.src = smallImg[1].src;
            }
            smallImg[2].onclick = function () {
                productImg.src = smallImg[2].src;
            }
            smallImg[3].onclick = function () {
                productImg.src = smallImg[3].src;
            }
        </script>
        </body>
        </html>
