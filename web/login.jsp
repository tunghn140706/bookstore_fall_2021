<%-- 
    Document   : login
    Created on : Sep 12, 2021, 8:03:51 PM
    Author     : cuongnm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="loginStyle.css">
    </head>
    <body style="background-color: #ffd6d6 ">
        <div class ="container">
            <div class = "row content">
                <div class = "col-md-6 mb-3">
                    <img src="images/sport.svg" class="img-fluid" alt="image">
                </div>
                <div class = "col-md-6">
                    <h3 class = "signin-text mb-3">Sign In</h3>
                    <form action="login" method="post">
                        <div class="form-group">
                            <label for="email">Username</label> 
                            <input type="text" name="username" class="form-control" value=${cookie.user.value}>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class = "form-control" value=${cookie.pass.value}>
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" name="checkbox" class="form-check-input" id="checkbox" value="remember">
                            <label class="form-check-label" for="checkbox">Remember Me</label>
                        </div>
                        <input class="btn btn-class" value="Login" type="submit">
                    </form>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

    </body>
</html>
